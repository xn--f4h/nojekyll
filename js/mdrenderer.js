/* 
 * markdown page renderer...
 *
 * assumed a layout_url and config_url have been previously set
 *
 * /!\ License: 
 *     you are not allowed to modify this code unless you
 *     you registered the url at which it is deployed
 *

deps: 
<script src="js-yaml.js"></script>
see also: https://github.com/nodeca/js-yaml
          https://www.jsdelivr.com/package/gh/nodeca/js-yaml?path=dist
<script src="https://cdn.jsdelivr.net/gh/nodeca/js-yaml@3.14.0/dist/js-yaml.js" integrity="sha256-AYw3qUq45Igx04UdvdYBzB39/ArYQKFATKmNkBTot/4=" crossorigin="anonymous"></script>

 */

/* 
console.dir(window.jsyaml)
import { yaml } from './js-yaml.js'
*/

// include showdown rendering script ...
var script = document.createElement('script');
    script.setAttribute('type','text/javascript');
    script.src = 'https://cdn.jsdelivr.net/npm/showdown@latest/dist/showdown.min.js'
    script.src = 'js/showdown.min.js'
    document.getElementsByTagName('head')[0].appendChild(script);
    var js1 = load(script);
// include js-yaml script ...
    script = document.createElement('script');
    script.setAttribute('type','text/javascript');
    script.src = 'https://cdn.jsdelivr.net/gh/nodeca/js-yaml@3.14.0/dist/js-yaml.js'
    script.src = 'js/js-yaml.js'
    document.getElementsByTagName('head')[0].appendChild(script);
    var js2 = load(script);

var brindexf = '/.brings/published/brindex.log'
var cfg;
var md_url;
var qmpub;

( async _ => {
 // load config json file
 cfg = await get_json(config_url);
 console.log('cfg:',cfg);
 // load layout file
 var layout = await get_file(layout_url)
 console.dir([layout]);
 // render md ...
 let mdnofm = 'md not rendered'
 let elems = document.getElementsByClassName('md');
 for (let e of Array.from(elems).reverse() ) {
   console.dir(e)
   console.log('e.src: ',e.src)
   if (typeof(e.src) != 'undefined') {
     md_url = e.src
     md = await get_file(md_url)
   } else if (typeof(e.dataset.url) != 'undefined') {
     console.log('e.dataset.url: ',e.dataset.url)
     md_url = e.dataset.url
     md = await get_file(md_url)
   } else {
     md = e.innerHTML;
   }
   console.log('md_url:',md_url)
   console.dir({'md':md});
   
   [frontmatter,mdnofm] = await extract_front_matter(md)

   console.dir({'md':[frontmatter,mdnofm]});

   // substi w/ cfg, front matter ...
   for (key in cfg) {
      mdnofm = mdnofm.replace(new RegExp('{{site.'+key+'}}','g'),cfg[key])
      layout = layout.replace(new RegExp('{{site.'+key+'}}','g'),cfg[key])
   }
   for (key in frontmatter) {
      console.log(key+': '+frontmatter[key]);
      mdnofm = mdnofm.replace(new RegExp('{{page.'+key+'}}','g'),frontmatter[key])
   }
   e.classList.remove('md'); // remove md classname once rendered !
   //console.log('e.classList:',e.classList)
   if (e.tagName == 'IFRAME') { // replace iframe w/ a div
     var ne = document.createElement('div');
     e.parentNode.replaceChild(ne, e);
     e.remove()
     e = ne
   }
   // render md div !
   e.innerHTML = await render(mdnofm)
  }

  console.dir(document)
  // replace content in layout with generated content
  let html = document.getElementsByTagName('html')[0]
  let content = document.getElementsByClassName('content')[0].innerHTML
  let docHTML = layout.replace('{{content}}',content)
  /*
  document.open();
  document.write(docHTML);
  document.close();
  */
  document.body.parentNode.innerHTML = docHTML
  let textarea = document.getElementById('editor')
  textarea.value = md;

})()


function back(ev) {
  document.location.href = cfg.prev-url;
}
function next(ev) {
  if (typeof(cfg.qmgen) != 'undefined') {
    document.location.href = 'http://localhost:8080/ipfs/'+cfg.qmgen
  } else {
    document.location.href = 'https://xn-f4h.gitlab.io/nojekyll'
  }
}
function edit(ev) {
  let el = document.getElementsByName('editor')[0]
  console.log('edit.el:',el)
  el.style.display = ''
}
async function togpin(ev) {
  console.log('toggle.pin')
}
function onclickClose(ev) {
  let el = document.getElementsByName('editor')[0]
  console.log('close.el:',el)
  el.style.display = 'none'
}

function share(ev) {
  alink = document.getElementById('new_url');
  let newqm = alink.title
  if (typeof(qmpub) != 'undefined') {
    document.location.href = cfg.pgw+'/ipfs/'+qmpub
  } else {
    alert('need to save before...')
  }
}
function menu(ev) {
  alert('no menu available (yet)');
}
async function onclickPublish(ev) {
  if (typeof(qmpub) != 'undefined') {
    let record = qmpub+': '+cfg.pubname
    let hash = await ipfs_append(record,'/.brings/published/brindex.log')
        hash = await ipfs_hash('/.brings')
    let json = await ipfs_api('name/publish?arg=/ipfs/'+hash+'/')
    console.log('url:',cfg.pgw+'/ipns/'+json.Name)
    console.log('name.publish:',json)
  }
  let url;
  if (typeof(alink.href) != 'undefined') {
    alink = document.getElementById('new_url');
    url = alink.url;
    document.location.href = alink.href;
  }
  return url;
}


async function onclickSave(ev) {
  console.log('ev:',ev)
  
  let textarea = document.getElementById('editor')
  let hash = await ipfs_add(textarea.value).catch(console.error);

  console.log('hash:',hash)
  let qm = cfg.qm;
  console.log('qm:',qm)

  console.log(document.location)
  let loc = document.location

  // update config ...
  cfg['prev-url'] = loc.href
  cfg['qm'] = hash
  if (typeof(cfg.qmgen) == 'undefined') {
    cfg.qmgen = 'QmSRTMktWj81REUjWteRfST2LonvCTW7RheKefipuKBYCS';
  }
  console.log('cfg:',cfg)
  let cfg_hash = await ipfs_add(JSON.stringify(cfg))
  console.log('cfg_hash:',cfg_hash)

  let ipath = getipath(cfg,md_url)
  // write down the new md file
  let newqm = await ipfs_update(ipath,hash)
      newqm = await ipfs_update('/ipfs/'+newqm+'/config.json',cfg_hash)

  console.log('newqm:',newqm)
  new_url = 'http://localhost:8080/ipfs/'+newqm+'/'
  alink = document.getElementById('new_url');
  alink.href= new_url
  alink.title= newqm
  alink.innerHTML= '/ipfs/'+newqm.substr(0,10)+'/'

  qmpub = newqm;

}

function getipath(cfg,url) {
  let name = url.substr(url.lastIndexOf('/')+1)
  let gw,ipath;
  let loc = cfg['prev-url'];
  if ( loc.match('/ipfs/')) {
    ipath = loc.pathname.replace(RegExp('[^/]+$'),name)
  } else if (loc.match(/\.(ip[fn]s)\./)) {
    matches = loc.match(RegExp('(https?://)(.*)\.(ip[fn]s)\.([^/]+)'));
    let key = matches[2]; key.replace(/\./g,'')
    gw = matches[1]+matches[4]
    ipath = '/'+matches[3]+'/'+key+'/' + url.replace(RegExp('[^/]+$'),name)
  } else {
    gw = cfg.pgw
    ipath = '/ipfs/'+cfg.qmgen+'/' + url.replace(RegExp('[^/]+$'),name)
  }
  console.log('ipath: ',ipath)
  console.log('url: ',cfg.pgw+ipath)
  return ipath
}

async function ipfs_update(path,hash) {
  const callee = 'ipfs_update'
  console.log(callee+'.path,hash: ',[path,hash])
  let p = path.lastIndexOf('/');
  let parent = path.substring(0,p);
  let name = path.substr(p+1);
  let ppath = await ipfs_resolve(parent);
  let phash = ppath.substr(ppath.indexOf('/',2)+1)
  console.log('phash: ',phash)
  // rm link
  let nhash = await ipfs_api('object/patch/rm-link?arg='+phash+'&arg='+name).
  then( obj => { console.log(callee+'.rm-link.obj: ',obj); return obj.Hash; });
  // add link
  nhash = await ipfs_api('object/patch/add-link?arg='+nhash+'&arg='+name+'&arg='+hash).
  then( obj => { console.log(callee+'.add-link.obj: ',obj); return obj.Hash; });

  return nhash
}

async function ipfs_append(data,file) { // easy way: read + create !
  let buf = await ipfs_api('files/read?arg='+file)
  console.log(buf)
  buf += data+"\n"
  let status = await ipfs_api('files/write?arg='+brindexf+'&create=true',buf);
  console.log('write.status:',status)
  let hash = await ipfs_hash(brindexf)
  console.log('hash: ',hash)
  return hash
}

function ipfs_hash(mfs_path) {
  return ipfs_api('files/stat?arg='+mfs_path)
  .then( json => { return json.Hash })
  .catch(console.error)

}
// empty file : QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH
// empty dir : QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nn
// empty obj : QmdfTbBqBPQ7VNxZEYEj14VmRuZBkqFbiwReogJgS1zR1n
function ipfs_rm(path) {
  console.log('path:',path)
  // remove the file and return new hash 
  let p = path.lastIndexOf('/');
  let parent = path.substring(0,p);
  let name = path.substr(p+1);
  console.log('rm.p,n:',[parent,name]) // TBD
}
async function ipfs_cp(hash,path) {
  let [ns, root, name] = path.split('/',3);
  console.log('ipfs_cp:',[ns,root,name])
  let rhash = await ipfs_resolve(root)
  
  return fetchJson(url).
  then( json => json.Path ).
  catch(console.error)
}




async function publish(hash,name) {

console.log('publish:',[hash,name])
if (name == 'self') {
 hkey = peerid
} else {
 hkey = get_hkey(name)
}
// ```sh
// brkey=$(ipms files stat --hash /.brings)
// ipms --offline name publish --allow-offline $brkey 1>/dev/null 2>&1
// ``` TBD
   return '/ipns/'+hkey
}


function get_hkey(symb) {
 // TBD
}


function ipfs_resolve(path) {
  return ipfs_api('resolve?arg='+path).
  then( json => json.Path ).
  catch(console.error)
}


function ipfs_add(buf) {
  //let query='add?file=bidon.blob&cid-version=0&cid-base=base36&only-hash=false'
  let query='add?file=bidon.blob&only-hash=false'
  return ipfs_api(query,buf).
  then( json => json.Hash ).
  catch(console.error)
}


function ipfs_api(query,data) {
  let api_url = (typeof(cfg.api_url) != 'undefined') ? cfg.api_url : 'http://localhost:5001/api/v0/';
  let url = api_url + query
  console.log('ipfs_api.url:',url)
  return fetchPost(url,data).
  then( resp => resp.text() ).
  then( text => {
    if (text.match(/^{/)) { // resp was a json object !
      json = JSON.parse(text);
      return json;
    } else {
      return text;
    }
  } ).
  catch(console.error)
}


function fetchJson(url) {
  return fetch(url, { method: "POST", mode: 'cors' }).
  then( resp => resp.json() ).
  catch(console.error)
}

function fetchPost(url, content) {
     if (typeof(content) != 'undefined') {
        let form = new FormData();
        form.append('file', content)
        return fetch(url, { method: "POST", mode: 'cors', body: form })
     } else {
        return fetch(url, { method: "POST", mode: 'cors' })
     }
}


async function render(md) {
   let html;
   if (typeof(showdown) == 'undefined') {
      // wait for script to load ...
      console.log('showdown script not loaded (js1)');
      return js1.
         then( _ => {
               var converter = new showdown.Converter();
               html = converter.makeHtml(md);
               return html;

               }).
         catch( err => {
               console.log(err); 
               return '<code><pre>'+md+'</code></pre>';
               })
   }
   var converter = new showdown.Converter();
   html = converter.makeHtml(md);
 return html;
}

async function extract_front_matter(buf) {
  let start = buf.indexOf("---",0);
  let stop = buf.indexOf("---\n",start+4);
  let header = buf.substring(start,stop) + '...';

  if (typeof(window.jsyaml) == 'undefined') {
     await js2.catch(console.error);
  }
  
  let fmatter = window.jsyaml.safeLoad(header);
  console.log('fmatter:',fmatter);
  let playload = buf.substr(stop+4);
  return [fmatter,playload];
}

function get_file(url) {
 return fetch(url)
 .then(resp => { return resp.text() })
 .catch(console.error)
}

function get_json(url) {
 return fetch(url)
 .then(resp => { return resp.json() })
 .catch(console.error)
}


function load(e) {
    //console.log('load: ',e); 
    return new Promise(function(resolve, reject) {
        e.onload = resolve
        e.onerror = reject
    });
}

