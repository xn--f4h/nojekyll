---
layout: default
var: value
---
# a simple site !

Indeed this is a very simple static website, rendered without [jekyll][1]
from a simple [markdown][2] &lt;div> (ex: [index.md](index.md)),
it uses a [layout][3] template and a [config][4] file;
the rendering is made possible with a javascript : [mdrenderer.js][5].


<!--
Indeed this is a very simple static website, compiled with [jekyll][1]
from a single [md-file][2], however jekyll is not even necessary when
we use [mdrenderer.js][5]
-->

```yaml
site.test: {{site.test}}
page.var: {{page.var}}
```

[1]: https://qwant.com/?q=jekyll+static+website+%23MYC+%26g
[2]: https://qwant.com/?q=markdown+file+format
[3]: layout.html
[4]: config.json
[5]: https://qwant.com/?q=mdrenderer.js+%26g

